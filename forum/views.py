from django.forms import ModelForm
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic.edit import CreateView

from .models import Thread, Comment


class IndexView(CreateView):
    """
    Lists threads and allows a user to create new threads
    """
    template_name = 'forum/index.html'
    model = Thread
    fields = ['subject', 'message']

    def get_context_data(self, **kwargs):
        """
        Gets the list of threads and adds it to the context data
        """
        context = super(IndexView, self).get_context_data(**kwargs)
        context["thread_list"] = self.model.objects.order_by('-updated_date')
        return context

    def get_success_url(self):
        """
        Redirect to the index on successful thread creation
        """
        return reverse('forum:index')


class ThreadDetailView(CreateView):
    """
    Views a thread and its related comments
    """
    template_name = 'forum/thread.html'
    model = Comment
    fields = ['message']

    def form_valid(self, form: ModelForm):
        """
        Associates the submitted form with the current thread
        """
        form.instance.thread = self.get_context_data()['thread']
        return super(ThreadDetailView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        """
        Gets the thread from the URL data and puts it in the context
        """
        context = super(ThreadDetailView, self).get_context_data(**kwargs)
        context['thread'] = get_object_or_404(Thread, pk=self.kwargs['pk'])
        return context

    def get_success_url(self):
        """
        Redirect back to the thread on successful post
        """
        return reverse('forum:view thread', args=(self.kwargs['pk'],))
