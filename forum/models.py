from django.db import models
from django.utils import timezone


class Thread(models.Model):
    subject = models.CharField(max_length=200)
    message = models.TextField()
    created_date = models.DateTimeField('thread creation date',
                                        auto_now_add=True)
    updated_date = models.DateTimeField('thread updated date', auto_now=True)

    def __str__(self):
        return self.subject


class Comment(models.Model):
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE)
    message = models.TextField()
    created_date = models.DateTimeField('comment creation date',
                                        auto_now_add=True)

    def save(self, *args, **kwargs):
        self.thread.updated_date = timezone.now()
        self.thread.save()

        super(Comment, self).save(*args, **kwargs)

    def __str__(self):
        return self.message
