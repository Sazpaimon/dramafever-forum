import datetime
from unittest.mock import patch, Mock

from django.test import TestCase
from django.urls import reverse

from .models import Thread


class IndexViewTests(TestCase):
    def test_index_with_no_threads_returns_appropriate_message(self):
        """
        Tests that when no threads exist, an informational message is shown
        """
        response = self.client.get(reverse('forum:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No threads are available.")
        self.assertQuerysetEqual(response.context['thread_list'], [])

    def test_index_will_show_threads_if_available(self):
        """
        Tests that the index will show threads if at least one exists
        """
        Thread.objects.create(subject="foo", message="bar")
        response = self.client.get(reverse('forum:index'))
        self.assertQuerysetEqual(
            response.context['thread_list'],
            ['<Thread: foo>']
        )

    def test_index_will_show_threads_in_correct_order(self):
        """
        Tests that the index will show threads ordered by their updated date
        """
        with patch('django.utils.timezone.now',
                   Mock(return_value=datetime.datetime(2000, 1, 1))):
            Thread.objects.create(subject="foo", message="bar")
        with patch('django.utils.timezone.now',
                   Mock(return_value=datetime.datetime(2000, 1, 2))):
            Thread.objects.create(subject="baz", message="qux")
        response = self.client.get(reverse('forum:index'))

        self.assertQuerysetEqual(
            response.context['thread_list'],
            ['<Thread: baz>', '<Thread: foo>']
        )

    def test_index_will_create_new_threads(self):
        """
        Tests that the index will create a new thread when requested
        """
        self.client.post(
            reverse('forum:index'),
            {"subject": "foo", "message": "bar"}
        )
        self.assertQuerysetEqual(
            Thread.objects.all(),
            ['<Thread: foo>']
        )


class ThreadDetailViewTests(TestCase):
    def test_thread_detail_with_invalid_thread_returns_404(self):
        """
        Tests that when a thread doesn't exist, a 404 will be returned
        """
        response = self.client.get(reverse('forum:view thread', args=(1,)))
        self.assertEqual(response.status_code, 404)

    def test_thread_detail_can_submit_new_comment(self):
        """
        Tests that the index will show threads if at least one exists
        """
        thread = Thread.objects.create(subject="foo", message="bar")
        response = self.client.post(
            reverse('forum:view thread', args=(thread.id,)),
            {"message": "foo"},
            follow=True
        )
        self.assertQuerysetEqual(
            response.context['thread'].comment_set.all(),
            ['<Comment: foo>']
        )
